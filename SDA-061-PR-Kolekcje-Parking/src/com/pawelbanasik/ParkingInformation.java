package com.pawelbanasik;

public class ParkingInformation {
	private String ticketId;
	private Long startTimestamp;
	private Long leaveTimestamp;
	// numer kasy w kt�rej rozliczono si�
	private int cashRegistryId;
	private boolean validated = false;
	public ParkingInformation(String ticketId) {
		this.ticketId = ticketId;
		this.startTimestamp = System.currentTimeMillis();
		this.leaveTimestamp = null;
	}
	/**
	 * Rozliczamy ticket.
	 * 
	 * @param cashRegistry
	 *            - numer kasy w kt�rej si� rozliczyli�my.
	 */
	public void validateTicket(int cashRegistry) {
		validated = true;
		cashRegistryId = cashRegistry;
		System.out.println("Ticket " + ticketId + " rozliczony w kasie " + cashRegistry);
	}
	public boolean isValidated() {
		return validated;
	}
	public void leave() {
		this.leaveTimestamp = System.currentTimeMillis();
	}
}