package com.pawelbanasik;

import java.util.Scanner;

public class Main {
	private static Company company;
	public static void main(String[] args) {
		company = new Company();
		Scanner sc = new Scanner(System.in);
		String inputLine = null;
		while (sc.hasNextLine()) {
			inputLine = sc.nextLine();
			if (inputLine.equals("quit")) {
				break;
			}
			parseLine(inputLine);
		}
	}
	private static void parseLine(String line) {
		String[] splits = line.split(" ");
		if (splits[0].equals("dodaj")) {
			parseDodaj(splits);
		} else if (splits[0].equals("wjazd")) {
			try {
				Integer parkingId = Integer.parseInt(splits[1]);
				company.carEntry(parkingId);
			} catch (NumberFormatException nfe) {
				System.err.println("Z�y format komendy.");
			}
		} else if (splits[0].equals("wyjazd")) {
			company.carLeave(splits[1]);
		} else if (splits[0].equals("rozlicz")) {
			try {
				Integer cashId = Integer.parseInt(splits[1]);
				// pierwszym parametrem validate jest numer kasy, drugim id samochodu
				company.validateTicket(cashId, splits[2]);
			} catch (NumberFormatException nfe) {
				System.err.println("Z�y format komendy.");
			}
		} else {
			System.err.println("Z�e wej�cie.");
		}
	}
	private static void parseDodaj(String[] parameters) {
		if (parameters[1].equals("parking")) {
			company.addParking();
		} else if (parameters[1].equals("samochod")) {
			try {
				Integer parkingNumber = Integer.parseInt(parameters[2]);
				company.addCar(parkingNumber);
			} catch (NumberFormatException|ArrayIndexOutOfBoundsException nfe) {
				System.err.println("Z�y format komendy.");
			}
		} else if (parameters[1].equals("kase")) {
			company.addCashRegister();
		}
	}
}
