package com.pawelbanasik;

import java.util.LinkedList;
import java.util.List;

public class Parking {
	private List<Car> inputQueue;
	public Parking() {
		inputQueue = new LinkedList<>();
	}
	public void addCar() {
		inputQueue.add(new Car());
		System.out.println("Samoch�d przed bramk�. Kolejka ma rozmiar:" + inputQueue.size());
	}
	/**
	 * Wjazd samochodu na parking.
	 * @return ticket id.
	 */
	public String carEntry() {
		// pobieram pierwszy samochod z kolejki
		Car firstInQueue = inputQueue.get(0);
		
		// usun pierwszy element
		inputQueue.remove(0);
		
		// bior� jego hashcode i zwracam go (to b�dzie ticketId)
		System.out.println("Car " + firstInQueue.hashCode() + " entered.");
		return String.valueOf(firstInQueue.hashCode());
	}
}
