package com.pawelbanasik;

import java.util.HashSet;
import java.util.Set;

public class CashRegister {
	private Set<ParkingInformation> validatedTickets;
	public CashRegister(){
		validatedTickets = new HashSet<>();
	}
	
	public boolean validateTicket(ParkingInformation information){
		if(!validatedTickets.contains(information)){
			System.out.println("Ticket zwalidowany w kasie.");
			validatedTickets.add(information);
			return true;
		}
		return false;
	}
}
